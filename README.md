# Latency.GG - Prototype Beacon
A prototype of the beacon written in Python (3.9) using Twisted.

Usage:
```bash
user@machine:~$ poetry run beacon --help
Usage: beacon [OPTIONS]

Options:
  --api TEXT             The URL of the Beacon API (e.g.
                         https://beacon.apis.latency.gg)
  --loc TEXT             The provider's name for the location that the beacon
                         is hosted in (e.g. eu-west-1a)
  --provider TEXT        The name of the hosting provider (e.g. maxihost)
  --ipv4 TEXT            The public IPv4 address of the beacon (e.g.
                         203.0.113.3)
  --ipv6 TEXT            The public IPv6 address of the beacon (e.g.
                         2001:db8::2021:6a4f:1)
  --token TEXT       The Beacon API authentication token
  --push_period INTEGER  The number of seconds between pushes
  --help                 Show this message and exit.
```

Supports use of ENVs as well:
```
BEACON_API
BEACON_LOCATION
BEACON_PROVIDER
BEACON_IPV4
BEACON_IPV6
BEACON_TOKEN
BEACON_PUSH_PERIOD
```

Installation userdata:
```bash
#!/bin/bash
sudo apt-get --yes update
sudo apt-get --yes install curl

# Set Beacon Environmental variables
mkdir /etc/systemd/system.conf.d
sudo cat >> /etc/systemd/system.conf.d/beacon.conf << EOF
[Manager]
DefaultEnvironment=BEACON_API="https://beacon.latency.gg" \
BEACON_LOCATION="location" \
BEACON_PROVIDER="provider" \
BEACON_IPV4=$(curl -4 --max-time 3 ifconfig.io) \
BEACON_IPV6=$(curl -6 --max-time 3 ifconfig.io) \
BEACON_TOKEN="true" \
BEACON_PUSH_PERIOD=5
EOF

sudo systemctl daemon-reload

# For alpha run:
curl -1sLf 'https://dl.cloudsmith.io/public/latencygg/beacon-alpha/setup.deb.sh' | sudo -E bash
# For prod run:
curl -1sLf 'https://dl.cloudsmith.io/public/latencygg/beacon/setup.deb.sh' | sudo -E bash

sudo apt update
sudo apt install --yes beacon

sudo systemctl enable beacon.service
sudo systemctl start beacon.service

```
