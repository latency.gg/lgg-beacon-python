"""
VERSION: Prototype - 0.2.0
LICENSE: WTFPL
COPYRIGHT: Latency.GG - 2021
"""

from typing import List, Optional

import lmdb  # type: ignore
from models import Sample

Ki = 1024
Mi = 1024 * Ki
Gi = 1024 * Mi
MILLISECONDS = 1000  # Convert from seconds to milliseconds


class Database:
    def __init__(self):
        self.env = lmdb.Environment(
            "../samples.lmdb",
            map_size=8 * Gi,
            subdir=True,
            readonly=False,
            metasync=True,
            sync=True,
            map_async=False,
            mode=493,
            create=True,
            readahead=True,
            writemap=False,
            meminit=True,
            max_readers=8,
            max_dbs=8,
            max_spare_txns=1,
            lock=True,
        )
        self.identity_index = self.env.open_db("identity_index".encode(), dupsort=True)
        self.timestamp_index = self.env.open_db(
            "timestamp_index".encode(), dupsort=True
        )
        self.incomplete_by_identity_db = self.env.open_db(
            "incomplete_by_identity_index".encode()
        )

    def write_txn(self):
        return self.env.begin(write=True)

    def read_txn(self):
        return self.env.begin()

    def get_samples_by_timestamp(self, txn, timestamp: int) -> List[Sample]:
        samples: List[Sample] = []
        with txn.cursor(self.timestamp_index) as cursor:
            cursor.set_key(int(timestamp // MILLISECONDS).to_bytes(8, byteorder="big"))
            for key in cursor.iternext_dup():
                samples.append(Sample.from_buffer_copy(txn.get(key)))
        return samples

    def put(self, txn, sample: Sample):
        txn.put(
            sample.ident + sample.seq.to_bytes(1, byteorder="big"),
            bytes(sample),
            db=self.incomplete_by_identity_db,
        )

    def pop_incomplete(self, txn, ident: bytes, seq: int) -> Optional[Sample]:
        with txn.cursor(self.incomplete_by_identity_db) as cursor:
            if cursor.set_key(ident + seq.to_bytes(1, byteorder="big")):
                sample = Sample.from_buffer_copy(cursor.value())
                cursor.delete()
                return sample
        return None

    def finalise(self, txn, sample: Sample):
        key = sample.ident + b"/" + (sample.timestamp_c0).to_bytes(8, byteorder="big")
        txn.put(key, bytes(sample))
        txn.put(sample.ident, key, db=self.identity_index)
        txn.put(
            (sample.timestamp_c0 // MILLISECONDS).to_bytes(8, byteorder="big"),
            key,
            db=self.timestamp_index,
        )
