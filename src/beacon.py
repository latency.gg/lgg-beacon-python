"""
VERSION: Prototype - 0.2.0
LICENSE: WTFPL
COPYRIGHT: Latency.GG - 2021
"""
import click
from click import INT
from nacl.signing import SigningKey  # type: ignore
from twisted.internet import reactor, task  # type: ignore

from api import register_beacon
from database import Database
from ping_protocol import Echo
from push_service import Push


@click.command()
@click.option(
    "--api",
    envvar="BEACON_API",
    prompt="Beacon API URL",
    help="The URL of the Beacon API (e.g. https://beacon.apis.latency.gg)",
)
@click.option(
    "--loc",
    envvar="BEACON_LOCATION",
    prompt="Location name",
    help="The provider's name for the location that the beacon is hosted in (e.g. eu-west-1a)",
)
@click.option(
    "--provider",
    envvar="BEACON_PROVIDER",
    prompt="Hosting provider name",
    help="The name of the hosting provider (e.g. maxihost)",
)
@click.option(
    "--ipv4",
    envvar="BEACON_IPV4",
    prompt="Public IPv4 address",
    help="The public IPv4 address of the beacon (e.g. 203.0.113.3)",
)
@click.option(
    "--ipv6",
    envvar="BEACON_IPV6",
    prompt="Public IPv6 address",
    prompt_required=False,
    help="The public IPv6 address of the beacon (e.g. 2001:db8::2021:6a4f:1)",
    default=None,
)
@click.option(
    "--token",
    envvar="BEACON_TOKEN",
    prompt="Beacon token",
    help="The Beacon API authentication token",
)
@click.option(
    "--push_period",
    envvar="BEACON_PUSH_PERIOD",
    prompt="Push period (seconds)",
    prompt_required=False,
    help="The number of seconds between pushes",
    type=INT,
    default=10,
)
def main(
    api: str,
    loc: str,
    provider: str,
    ipv4: str,
    ipv6: str,
    token: str,
    push_period: int,
):
    hsk = SigningKey.generate()
    uuid, api_hsk_pub = register_beacon(api, loc, provider, "0.2.0", ipv4, ipv6, token)
    database = Database()
    echo_4 = Echo(hsk, api_hsk_pub, database)
    echo_6 = Echo(hsk, api_hsk_pub, database)
    pusher = Push(api, uuid, token, database)
    push_loop = task.LoopingCall(pusher.in_thread, reactor)
    push_loop.start(push_period)
    reactor.listenUDP(9998, echo_4)
    reactor.listenUDP(9999, echo_6, interface="::")
    reactor.run()
